from base.appium_driver import AppiumDriver
import utilities.custome_logger as cl
import logging
import time


class FirstSetup(AppiumDriver):
    log = cl.customLogger(logging.DEBUG)

    def __init__(self, driver):
        AppiumDriver.__init__(driver)
        self.driver = driver

    # locator
    _save_button = "org.gnucash.android:id/btn_save"
    _text_ = "android:id/title"
    _dis_button = "android:id/button1"
    _account_page_ = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout" \
                     "/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout" \
                     "/android.view.ViewGroup/android.support.v4.view.ViewPager/android.widget.RelativeLayout/android" \
                     ".support.v7.widget.RecyclerView/android.widget.FrameLayout[" \
                     "1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.TextView[1] "

    _file_page_ = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout" \
                  "/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android" \
                  ".view.ViewGroup/android.widget.TextView "

    def setup_radio_xparh(self, i='1'):
        radio_xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout" \
                      "/android.view.ViewGroup/android.widget.FrameLayout[" \
                      "2]/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout" \
                      "/android.widget.ListView/android.widget.CheckedTextView[" + i + "]"
        return radio_xpath

    def verifySetup(self):
        # page 1
        # verify that its first time setup
        result = self.isElementPresent(self._text_)
        return result

    def click_button(self, button):
        # start for setup and click next button
        self.elementClick(button)

    def radio_button_option(self, pTarget):
        # page 2
        # select EUR as default currency
        self.waitForElement(pTarget, "xpath")
        self.elementClick(pTarget, "xpath")

    def account_page_items_visibility_check(self, locator):
        # check the account home page type
        self.waitForElement(locator, "xpath")
        result = self.elementPresenceCheck(locator, "xpath")
        return result

    def account_page_items_is_visible(self, locator):
        # check the account home page type
        result = self.isElementPresent(locator, "xpath")
        return result

    def intro(self, account):
        self.verifySetup()
        self.click_button(self._save_button)
        self.radio_button_option(self.setup_radio_xparh('2'))
        self.click_button(self._save_button)
        self.radio_button_option(account)
        self.click_button(self._save_button)
        self.radio_button_option(self.setup_radio_xparh('1'))
        self.click_button(self._save_button)
        self.verifySetup()
        self.click_button(self._save_button)
        time.sleep(8)
        self.click_button(self._dis_button)
