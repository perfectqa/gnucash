import pytest
from base.driverFactory import driver_instance


@pytest.yield_fixture()
def setUp():
    print("Running method level setUp")
    yield
    print("Running method level tearDown")


@pytest.fixture(scope="class")
def oneTimeSetUp(request):
    print("Running one time setUp")
    driver = driver_instance()


    if request.cls is not None:
        request.cls.driver = driver

    yield driver
    driver.quit()
    print("Running one time tearDown")


def pytest_addoption(parser):
    parser.addoption("--osType", help="Type of operating system")


@pytest.fixture(scope="session")
def osType(request):
    return request.config.getoption("--osType")
