import time
import unittest
import pytest
from pages.firstsetup.intro import FirstSetup


@pytest.mark.usefixtures("oneTimeSetUp")
class User_Setup_Account(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self, oneTimeSetUp):
        self.sp = FirstSetup(self.driver)

    @pytest.mark.run(order=1)
    def test_valid_user_file_setup(self):
        self.sp.intro(self.sp.setup_radio_xparh('3'))
        time.sleep(3)
        locator = self.sp._account_page_
        result = self.sp.account_page_items_is_visible(locator)
        assert result == False


