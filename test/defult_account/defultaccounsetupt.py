import unittest
import pytest
from pages.firstsetup.intro import FirstSetup


@pytest.mark.usefixtures("oneTimeSetUp")
class Defult_Account_Setup(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self, oneTimeSetUp):
        self.sp = FirstSetup(self.driver)

    @pytest.mark.run(order=1)
    def test_valid_default_setup(self):
        self.sp.intro(self.sp.setup_radio_xparh('1'))
        locator = self.sp._account_page_
        result = self.sp.account_page_items_visibility_check(locator)
        assert result == True



