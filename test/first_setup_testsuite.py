import unittest
from test.defult_account.defultaccounsetupt import Defult_Account_Setup
from test.user_account_from_file.useraccountfromfile import User_File_Setup_Account
from test.user_setup_account.useraccouuntsetup import User_Setup_Account


tc1 = unittest.TestLoader().loadTestsFromTestCase(Defult_Account_Setup)
tc2 = unittest.TestLoader().loadTestsFromTestCase(User_File_Setup_Account)
tc3 = unittest.TestLoader().loadTestsFromTestCase(User_Setup_Account)

smokeTest = unittest.TestSuite([tc1, tc2, tc3])
unittest.TextTestRunner(verbosity=2).run(smokeTest)