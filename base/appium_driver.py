from traceback import print_stack
from appium.webdriver.webelement import By
from selenium.common.exceptions import *
from selenium.webdriver.support.ui import WebDriverWait
from appium.common import exceptions as EC
import utilities.custome_logger as cl
import logging


class AppiumDriver():
    log = cl.customLogger(logging.DEBUG)

    def __int__(self, driver):
        self.driver = driver

    def getByType(self, locatorType):
        locatorType = locatorType.lower()
        if locatorType == "id":
            return By.ID
        elif locatorType == "name":
            return By.NAME
        elif locatorType == "xpath":
            return By.XPATH
        elif locatorType == "CSS":
            return By.CSS_SELECTOR
        elif locatorType == "link":
            return By.LINK_TEXT
        else:
            self.log.info("Locator type" + locatorType + "not supported")
        return False

    def getElement(self, locator, locatorType="id"):
        element = None
        try:
            locatorType = locatorType.lower()
            byType = self.getByType(locatorType)
            element = self.driver.find_element(byType, locator)
            self.log.info("Element not found with locatorType: " + locator +
                          " and locatorType: " + locatorType)
        except:
            self.log.info("Element not found with locatorType: " + locator +
                          " and locatorType: " + locatorType)
        return element

    def elementClick(self, locator, locatorType="id"):
        try:
            element = self.getElement(locator, locatorType)
            element.click()
            self.log.info("Clicked on the element with locator: " + locator + "locatorType: " + locatorType)
        except:
            self.log.info("Cannot click on the element with locator: " + locator + "locatorType: " + locatorType)
            print_stack()

    def sendKey(self, data, locator, locatorType="id"):
        try:
            element = self.getElement(locator, locatorType)
            element.send_keys(data)
            self.log.info("Sent data on the element with locator: " + locator + "locatorType: " + locatorType)
        except:
            self.log.info("Cannot send data on the element with locator: " + locator + "locatorType: " + locatorType)
            print_stack()

    def isElementPresent(self, locator, locatorType="id"):
        try:
            element = self.getElement(locatorType, locator)
            if element is not None:
                self.log.info("Element found")
                return True
            else:
                self.log.info("Element not found")
                return False
        except:
            self.log.info("Element not found")

    def elementPresenceCheck(self, locator, byType):
        try:
            elementList = self.driver.find_elements(byType, locator)
            if len(elementList) > 0:
                self.log.info("Element found")
                return True
            else:
                self.log.info("Element not found")
        except:
            self.log.info("Element not found")
            return False

    def waitForElement(self, locator, locatoreType="id", timeout=10, pollFrequency=0.5):
        element = None
        try:
            byType = self.getByType(locatoreType)
            self.log.info("Waiting for ::" + str(timeout) + " :: s for element to be clickable")
            wait = WebDriverWait(self.driver, timeout, poll_frequency=pollFrequency,
                                 ignored_exceptions=[NoSuchElementException,
                                                     ElementNotVisibleException,
                                                     ElementNotSelectableException])
            element = wait.until(EC.element_to_be_clickable((byType, locator)))
            self.log.info("Element appeared on the web page")
        except:
            self.log.info("Element not appeared on the web page")
            print_stack()
            return element

    # def getCurrentActivity(self):
    #     activity = self.driver.current_activity
    #     return activity
    #
    # def checkIfActivityIsPresent(self, activity):
    #     if self.getCurrentActivity() == activity:
    #         self.log.info("current activity is match")
    #         return True
    #     else:
    #         self.log.info("current activity is not match")
    #         return False