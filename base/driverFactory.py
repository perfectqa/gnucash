from appium import webdriver


def driver_instance():
    dc = {'app': 'c:\\GnucashAndroid_v2.4.0.apk', 'packageName': 'org.gnucash.android',
          'appActivity': 'org.gnucash.android.ui.account.AccountsActivity', 'platformName': 'Android',
          'deviceName': 'emulator-5554', 'autoGrantPermissions': 'true',
          'appWaitActivity': 'org.gnucash.android.ui.*'}
    driver = webdriver.Remote("http://localhost:4723/wd/hub", dc)

    return driver
