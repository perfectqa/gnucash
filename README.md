INSTALLATION REQUIERMENTS:

1- npm

2- Appium Server: http://appium.io/docs/en/about-appium/getting-started/

3- Android studio

4- Python3

5- Pycharm

_STEPS TO RUN THE DEMO:_

Clone this git repository

**git clone**

Start Appium Server using Appium Desktop installed on your PC. The project expects the Appium Server to run on localhost:4723. If you run the server to a different host and port. Please change the code.

Import the cloned project in Pycharm (In Pycharm menu, navigate to File > Open or "Open" if no projects are open)

Modify the following variables in the code if necessary

Open **driverFactory.py** and modify the following if necessary,

Appium Server listening host and port.

> driver = webdriver.Remote("http://localhost:4723/wd/hub", dc)

Modify the following if necessary,

Path of the application 

'app': 'c:\\GnucashAndroid_v2.4.0.apk'

Device name (After executing ADB devices. See "Android device recognition" in the blogs)

deviceName = 'emulator-5554'